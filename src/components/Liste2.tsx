import React from "react";
import { IonList, IonItem } from "@ionic/react";

interface Props2 {
  data :{
    Country : string;
    TotalDeaths : number;
  }[];
  filtre : string;
}

const Liste2 = (props: Props2) => (
  <>
    {props.data ? (
      <IonList>
        {props.data
          .filter((element) => !props.filtre || element.Country.includes(props.filtre))
          .map((element) => (
            <IonItem>
              {element.Country} ({element.TotalDeaths})
            </IonItem>
          ))}
      </IonList>
    ) : (
      <div>Chargement en cours</div>
    )}
  </>
);

export default Liste2;
