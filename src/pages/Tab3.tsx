import React, { useState } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonList, IonItem, IonLabel, IonCheckbox, IonItemDivider, IonButton, IonIcon, IonAlert } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab3.css';
import { addOutline, addCircleOutline } from 'ionicons/icons';


const checkboxList1 = [
  { val: 'Pâtes', isChecked: true },
  { val: 'Sauce tomate', isChecked: false },
  { val: 'Champignons', isChecked: false }
];

const checkboxList2 = [
  { val: 'Salade', isChecked: false },
  { val: 'Petits pois', isChecked: false },
  { val: 'Haricots verts', isChecked: true },
  { val: 'Lentilles', isChecked: true },
  { val: 'Yaourts', isChecked: false }
];

const Tab3: React.FC = () => {

  const [checked, setChecked] = useState(false);
  const [showAlert4, setShowAlert4] = useState(false);
  
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Ma to do list</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
      <IonList>
          <IonItemDivider>Liste 1</IonItemDivider>
          {checkboxList1.map(({ val, isChecked }, i) => (
            <IonItem key={i}>
              <IonLabel>{val}</IonLabel>
              <IonCheckbox slot="end" value={val} checked={isChecked} color="dark"/>
            </IonItem>
          ))}
          <IonButton color="dark" expand="full" fill="clear" onClick={() => setShowAlert4(true)}>
            Ajouter un élement à la liste
          </IonButton>
          <IonAlert
          isOpen={showAlert4}
          onDidDismiss={() => setShowAlert4(false)}
          header={'Ajouter un élément à la liste'}
          inputs={[
            {
              name: 'name',
              type: 'text',
              placeholder: "Entrez l'élément à ajouter",
              handler: () => {
                console.log("toto");
              }
            }
          ]}
          buttons={[
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            },
            {
              text: 'Ok',
              handler: () => {
                console.log('Confirm Ok');
                checkboxList1.push({val : "Nouvel élément", isChecked : false});
              }
            }
          ]}
        />

          <IonItemDivider>Liste 2</IonItemDivider>
          {checkboxList2.map(({ val, isChecked }, i) => (
            <IonItem key={i}>
              <IonLabel>{val}</IonLabel>
              <IonCheckbox slot="end" value={val} checked={isChecked} color="dark"/>
            </IonItem>
          ))}
          <IonButton color="dark" expand="full" fill="clear" onClick={() => setShowAlert4(true)}>
            Ajouter un élement à la liste
          </IonButton>
          <IonAlert
          isOpen={showAlert4}
          onDidDismiss={() => setShowAlert4(false)}
          header={'Ajouter un élément à la liste'}
          inputs={[
            {
              name: 'name',
              type: 'text',
              placeholder: "Entrez l'élément à ajouter"
            }
          ]}
          buttons={[
            {
              text: 'Annuler',
              role: 'cancel',
              cssClass: 'secondary',
              handler: () => {
                console.log('Confirm Cancel');
              }
            },
            {
              text: 'Ok',
              handler: () => {
                console.log('Confirm Ok');
                checkboxList2.push({val : "Nouvel élément", isChecked : false});
              }
            }
          ]}
        />
        </IonList>
      </IonContent>
    </IonPage>
  );
};

export default Tab3;
