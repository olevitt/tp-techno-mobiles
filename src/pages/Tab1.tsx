import React, { useState, useEffect } from 'react';
import { IonContent, IonHeader, IonPage, IonTitle, IonToolbar, IonText } from '@ionic/react';
import ExploreContainer from '../components/ExploreContainer';
import './Tab1.css';
import Liste from '../components/Liste';
import fakeData from '../components/fakedata.json';
import Geoloc from '../components/Geoloc';
import Footer from '../components/Footer';
/*

const Header: React.FC = () => {
  return <div>Ma super appli</div>;
};
*/

/*
const ComposantFiltre: React.FC = ({onChange}) => {
  return (
    <>
      <IonText
        id="standard-basic"
        label="Filtre à appliquer"
        onChange={(event) => onChange(event.target.value)}
      />
    </>
  );
};

const Body: React.FC = () => {
  const [data, setData] = useState(undefined);
  const [filtre, setFiltre] = useState(undefined);

  useEffect(() => {
    fetch("https://api.covid19api.com/summary")
      .then((resp) => resp.json())
      .then((json) => {
        setData(json);
        console.log(json);
      });
  }, []);

  return (
    <>
      <ComposantFiltre onChange={(newValue) => setFiltre(newValue)} />
      <Liste data={data ? data.Countries : undefined} filtre={filtre} />
    </>
  );
};

*/




/*
constructor(private vibration: Vibration) { }

...

// Vibrate the device for 0,1 second
// Duration is ignored on iOS.
this.vibration.vibrate(100);
*/


const Tab1: React.FC = () => {
  return (
    <IonPage>
      <IonHeader>
        <IonToolbar>
          <IonTitle>Nombre de cas de COVID-19 confirmés par pays</IonTitle>
        </IonToolbar>
      </IonHeader>
      <IonContent>
        <IonHeader collapse="condense">
          <IonToolbar>
            <IonTitle size="large">Tab 1</IonTitle>
          </IonToolbar>
        </IonHeader>
        <Geoloc/>
        <Liste data={fakeData.Countries} filtre="" /> 
        <Footer />
      </IonContent>
    </IonPage>
  );
};

export default Tab1;
